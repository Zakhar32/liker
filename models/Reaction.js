class Reaction {
  constructor(
    id,
    name,
    channelId,
    messageId,
    reactions,
    isActive,
    isFinish,
    tokens
  ) {
    this.id = id;
    this.name = name;
    this.channelId = channelId;
    this.messageId = messageId;
    this.reactions = reactions;
    this.isActive = isActive;
    this.isFinish = isFinish;
    this.tokens = tokens;
  }
}

module.exports = Reaction;
