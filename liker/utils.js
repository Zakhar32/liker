const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
};

const delay = (ms) => {
  return new Promise((resolve) =>
    setTimeout(() => {
      const val = Math.trunc(Math.random() * 100);
      resolve(val);
    }, ms)
  );
};

const getMax = (reactions, length) => {
  let max = 0;

  for (const item of reactions) {
    if ((item.part / 100) * length > max) {
      max = (item.part / 100) * length;
    }
  }

  return max;
};

module.exports = {
  getRandomInt,
  delay,
  getMax,
};
