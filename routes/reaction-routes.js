const express = require("express");
const {
  addReaction,
  updateReaction,
  getAllReactions,
  getReactionProgress,
} = require("../controllers/reactionController");

const router = express.Router();

router.post("/reactions", addReaction);
router.get("/reactions", getAllReactions);
router.put("/reactions/:id", updateReaction);
router.get("/reactions/:id/progress", getReactionProgress);

module.exports = {
  routes: router,
};
