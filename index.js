const express = require("express");
const fetch = require("node-fetch");
const config = require("config");
const cors = require("cors");
const bodyParser = require("body-parser");
const reactionRoutes = require("./routes/reaction-routes");

const PORT = process.env.PORT ?? 5000;

const app = express();

app.use(express.json());
app.use(cors());
app.use(bodyParser.json());

app.use("/api", reactionRoutes.routes);

const start = async () => {
  try {
    app.listen(PORT, async () => {
      console.log("server started...");
      console.log("Port", PORT);
      const resp = await fetch(`http://localhost:${PORT}/api/reactions`);
      console.log(resp.ok, "TEST REQUEST");
      setInterval(async () => {
        const res = await fetch(`http://localhost:${PORT}/api/reactions`);
        console.log(res.ok, "TEST REQUEST");
      }, 1000 * 60 * 10);
    });
  } catch (e) {
    console.log("Server error:", e);
    process.exit(1);
  }
};

start();
